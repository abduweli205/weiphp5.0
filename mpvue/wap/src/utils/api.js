import { post, get } from './index'
const _SESSID = window.localStorage.getItem("PHPSESSID")
const api = {
  // 获取会员卡
  getCard: () => post('card/api/me', {
    PHPSESSID: _SESSID
  }),
  // 获取验证码
  getVerifyCode: (phone) => post('card/api/send_sms_code', {
    PHPSESSID: _SESSID,
    to: Number(phone)
  }),
  // 会员通知
  getInform: () => post('card/api/member_inform', {
    PHPSESSID: _SESSID
  }),
  // 会员有礼
  getGiftInfo: () => post('card/api/custom_privilege', {
    PHPSESSID: _SESSID
  }),
  // 积分兑换
  getScoreExchange: () => post('card/api/score_exchange', {
    PHPSESSID: _SESSID
  }),
  // 会员中心
  getMemberInfo: () => post('card/api/member_center', {
    PHPSESSID: _SESSID
  }),
}

export {
  api
}